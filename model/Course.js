const mongoose = require('mongoose');



// Start of Course Model

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,  // What would be the format for this?
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, 'UserId is required']
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);

// End of Course Model




