const Course = require("../model/Course");


// Create a new course
module.exports.addCourse = async (reqBody, data) => {

	if(data){

			// Creates a variable "newCourse" and instantiaties a new "Course" object using the mongoose model
			let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});

		// Save the created object to the database

		return newCourse.save().then((course, error) => {
			if(error){
				return false;
			}

			else{
				return true;
			}
		})
	}

	else{
		return false;
	}
}



// Controller method for retrieveing all the courses
module.exports.getAllCourses = () => {
	return Course.find({})
	.then(result => {
		return result;
	})
}



// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true})
	.then(result => {
		return result;
	})
}




// Retrieve spcific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId)
	.then(result => {
		return result;
	})
}



// Update a course
module.exports.updateCourse = async (reqParams, reqBody, isAdmin) => {
	// Specify the fields/properties of the document to be updated

	if(isAdmin){
		let updatedCourse = {
			name: reqBody.name,
			decription: reqBody.description,
			price: reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
		.then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}

	else{
		return `Not authorized to update`;
	}
}




// Archive a Course - s35
module.exports.archiveCourse = async (reqParams, reqBody, user) => {
	// Specify the fields/properties of the document to be updated

	if(user.isAdmin){
		let updatedCourse = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams, updatedCourse)
		.then((course, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}

	else{
		return `Not authorized to update`;
	}
}