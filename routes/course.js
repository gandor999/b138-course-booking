const express = require('express');
const router = express.Router();
const courseController = require('../controllers/course');
const auth = require('../auth');

// Route for creating a course
// Adding authorization
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	courseController.addCourse(req.body, userData.isAdmin)
	.then(resultFromController => res.send(resultFromController))
	
});


// Route for retrieving all the routes
router.get("/all", (req, res) => {
	courseController.getAllCourses()
	.then(resultFromController => res.send(resultFromController))
});



// Route for retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive()
	.then(resultFromController => res.send(resultFromController))
});



// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params)
	.then(resultFromController => res.send(resultFromController))
})



// Route for updating a course
router.put("/:courseId", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	courseController.updateCourse(req.params, req.body, userData.isAdmin)
	.then(resultFromController => res.send(resultFromController))
})



// Route for Archiving - s35
router.put("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	
	courseController.archiveCourse(req.params.courseId, req.body, userData)
	.then(resultFromController => res.send(resultFromController))
})




module.exports = router;